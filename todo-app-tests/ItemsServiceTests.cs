﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using todo_aspnetmvc.MapProfiles;
using todo_aspnetmvc.Models;
using todo_aspnetmvc.Services;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_app_tests
{
    [TestFixture]
    public class ItemsServiceTests
    {
        static DbContextOptions<AppDbContext> options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "ToDoList")
            .Options;

        private AppDbContext _appDbContext;
        private IItemService _itemsService;
        private IFolderService _foldersService;
        private IMapper _mapper;

        [SetUp]
        public void SetUp()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<MapProfile>());
            _mapper = new Mapper(config);
            _appDbContext = new AppDbContext(options);
            _appDbContext.Database.EnsureDeleted();
            _itemsService = new ItemService(_appDbContext, _mapper);
            _foldersService = new FolderService(_appDbContext, _mapper);
        }

        [Test]
        public async Task GetItems_ShouldReturnAllItemsBFolderId()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            await _itemsService.Create(new ItemViewModel
            {
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                FolderId = folderId
            });

            await _itemsService.Create(new ItemViewModel
            {
                Name = "Task 2",
                Description = "task 2 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                FolderId = folderId
            });
            var result = await _itemsService.GetFolderItems(folderId, new ItemFilterParameters());
            Assert.That(result, Has.Count.EqualTo(2));
        }

        [Test]
        public async Task GetItem_ShouldReturnItemByTaskId()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = await _itemsService.Create(new ItemViewModel
            {
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                FolderId = folderId
            });

            var result = await _itemsService.GetItem(taskId);
            Assert.That(result, Is.Not.Null);
            Assert.That(taskId, Is.EqualTo(result.Id));
        }

        [Test]
        public async Task CreateItem_ShouldCreateNewItem()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = await _itemsService.Create(new ItemViewModel
            {
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                FolderId = folderId
            });

            var result = await _itemsService.GetItem(taskId);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(taskId));
        }

        [Test]
        public async Task UpdateItem_ShouldUpdateItem()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = await _itemsService.Create(new ItemViewModel
            {
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                FolderId = folderId
            });

            var task = await _itemsService.GetItem(taskId);
            task.Name = "New name Task 2";

            await _itemsService.Update(task);

            var result = await _itemsService.GetItem(taskId);

            Assert.That(result.Name, Is.EqualTo("New name Task 2"));
        }

        [Test]
        public async Task DeleteItem_ShouldDeleteItem()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = await _itemsService.Create(new ItemViewModel
            {
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                FolderId = folderId
            });

            var task = await _itemsService.GetItem(taskId);
            Assert.That(task, Is.Not.Null);

            await _itemsService.Delete(taskId);
            var result = await _itemsService.GetItem(taskId);

            Assert.That(result, Is.Null);
        }
    }
}