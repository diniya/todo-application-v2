﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using todo_aspnetmvc.MapProfiles;
using todo_aspnetmvc.Models;
using todo_aspnetmvc.Services;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_app_tests
{
    [TestFixture]
    public class FolderServiceTests
    {
        static DbContextOptions<AppDbContext> options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "ToDoList")
            .Options;

        private AppDbContext _appDbContext;
        private IFolderService _foldersService;
        private IMapper _mapper;

        [SetUp]
        public void SetUp()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<MapProfile>());
            _mapper = new Mapper(config);
            _appDbContext = new AppDbContext(options);
            _appDbContext.Database.EnsureDeleted();
            _foldersService = new FolderService(_appDbContext, _mapper);
        }

        [Test]
        public async Task GetFolders_ShouldReturnAllLists()
        {
            await _foldersService.Create(new FolderViewModel
            {
                Id = Guid.NewGuid(), Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });
            await _foldersService.Create(new FolderViewModel
            {
                Id = Guid.NewGuid(), Name = "List 2", IsHidden = false, Icon = "img/calendar.png"
            });

            var result = await _foldersService.GetFolders(new ItemFilterParameters {IsHidden = false});
            Assert.That(result, Has.Count.EqualTo(2));
        }

        [Test]
        public async Task CreateAndGetFolder_ShouldReturnListById()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var result = await _foldersService.GetFolder(folderId);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(folderId));
        }

        [Test]
        public async Task Update_ShouldUpdateFolderById()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });
            var list = await _foldersService.GetFolder(folderId);

            list.Name = "List 2";
            await _foldersService.Update(folderId, list);

            var result = await _foldersService.GetFolder(folderId);

            Assert.That(result.Name, Is.EqualTo("List 2"));
        }

        [Test]
        public async Task Delete_ShouldDeleteFolderById()
        {
            var folderId = await _foldersService.Create(new FolderViewModel
            {
                Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var result = await _foldersService.GetFolder(folderId);
            Assert.That(result, Is.Not.Null);

            await _foldersService.Delete(folderId);
            result = await _foldersService.GetFolder(folderId);
            Assert.That(result, Is.Null);
        }
    }
}