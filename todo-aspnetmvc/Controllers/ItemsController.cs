﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using todo_aspnetmvc.Models;
using todo_aspnetmvc.Services;

namespace todo_aspnetmvc.Controllers
{
    [Route("[controller]/[action]")]
    public class ItemsController : Controller
    {
        private readonly IItemService _itemsService;

        public ItemsController(IItemService itemsService)
        {
            _itemsService = itemsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTodayItems()
        {
            var findTask = await _itemsService.GetTodayItems();
            return View(findTask);
        }

        public async Task<IActionResult> EditItem(Guid? taskId, Guid? folderId)
        {
            if (taskId.HasValue)
            {
                var task = await _itemsService.GetItem(taskId.Value);
                return View(task);
            }

            return View(new ItemViewModel {FolderId = folderId.Value});
        }

        [HttpPost]
        public async Task<IActionResult> SaveTask([FromForm] ItemViewModel task)
        {
            if (!ModelState.IsValid)
            {
                return View("EditItem", task);
            }

            if (task.Id == Guid.Empty)
            {
                task.Id = await _itemsService.Create(task);
                return RedirectToAction("Get", "Folders", new {folderId = task.FolderId});
            }

            var taskEntity = await _itemsService.GetItem(task.Id);
            taskEntity.Name = task.Name;
            taskEntity.Description = task.Description;
            taskEntity.CompletionDate = task.CompletionDate;

            await _itemsService.Update(taskEntity);
            return RedirectToAction("Get", "Folders", new {folderId = taskEntity.FolderId});
        }

        [HttpPost("{taskId:guid}")]
        public async Task<IActionResult> UpdateStatus([FromRoute] Guid taskId, [FromForm] UpdateTaskViewModel viewModel)
        {
            await _itemsService.UpdateStatus(taskId, viewModel.Status);
            return Ok();
        }

        /// <summary>
        /// Method deletes task by Id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpPost("{taskId:guid}")]
        public async Task<IActionResult> DeleteTask([FromRoute] Guid taskId)
        {
            var task = await _itemsService.GetItem(taskId);
            await _itemsService.Delete(taskId);
            return RedirectToAction("Get", "Folders", new {folderId = task.FolderId});
        }
    }
}