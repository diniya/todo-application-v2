﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using todo_aspnetmvc.Services;
using todo_domain_entities.Models;
using todo_aspnetmvc.Models;

namespace todo_aspnetmvc.Controllers
{
    public class FoldersController : Controller
    {
        private readonly IFolderService _foldersService;

        public FoldersController(IFolderService foldersService)
        {
            _foldersService = foldersService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(bool isHidden = false)
        {
            var folder = await _foldersService.GetFolders(new ItemFilterParameters {IsHidden = isHidden});
            return View(folder);
        }

        [HttpGet("{folderId:guid}")]
        public async Task<IActionResult> Get([FromRoute] Guid folderId)
        {
            FolderViewModel findTask = await _foldersService.GetFolder(folderId);

            return View(findTask);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid? folderId)
        {
            if (folderId.HasValue)
            {
                FolderViewModel findTask = await _foldersService.GetFolder(folderId.Value);
                return View(findTask);
            }

            return View(new FolderViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromForm] FolderViewModel folder)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", folder);
            }

            if (folder.Id == Guid.Empty)
            {
                folder.Id = await _foldersService.Create(folder);
            }
            else
            {
                await _foldersService.Update(folder.Id, folder);
            }

            return RedirectToAction("Get", "Folders", new {folderId = folder.Id});
        }

        [HttpPost]
        public async Task<IActionResult> Copy(Guid folderId)
        {
            var newFolderId = await _foldersService.Copy(folderId);
            return RedirectToAction("Get", "Folders", new {folderId = newFolderId});
        }

        [HttpPost("{folderId:guid}")]
        public async Task<IActionResult> DeleteList([FromRoute] Guid folderId)
        {
            await _foldersService.Delete(folderId);
            return RedirectToAction("Index", "Folders");
        }
    }
}