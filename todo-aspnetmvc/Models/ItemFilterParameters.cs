﻿using System;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Models
{
    public class ItemFilterParameters : SortParameters
    {
        public string? Name { get; set; }
        public bool IsHidden { get; set; }
        public StatusItem? Status { get; set; }
        
        public DateTime? CompletionDate{ get; set; }
       
    }
}