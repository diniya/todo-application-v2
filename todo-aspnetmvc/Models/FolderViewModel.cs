﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Models
{
    public class FolderViewModel
    {
        public Guid Id { get; set; }
        [Required] public string Name { get; set; }
        public bool IsHidden { get; set; }
        public string? Icon { get; set; }
        public List<ItemViewModel> Items { get; set; } = new List<ItemViewModel>();
    }
}