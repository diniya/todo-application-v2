﻿using System;
using System.ComponentModel.DataAnnotations;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Models
{
    public class ItemViewModel
    {
        public Guid Id { get; set; }

        [Required] public string Name { get; set; }
        public string? Description { get; set; }
        public StatusItem Status { get; set; }
        public DateTime? StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? CompletionDate { get; set; }

        public Guid FolderId { get; set; }
        public FolderViewModel? Folder { get; set; }
    }
}