﻿using System;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Models
{
    public class UpdateTaskViewModel
    {
        public Guid FolderId { get; set; }
        public StatusItem Status { get; set; }
    }
}