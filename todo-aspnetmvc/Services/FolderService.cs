﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using todo_aspnetmvc.Models;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Services
{
    public class FolderService : IFolderService
    {
        private readonly AppDbContext _dbContext;
        private readonly IMapper _mapper;

        public FolderService(AppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<List<FolderViewModel>> GetFolders(ItemFilterParameters filterParameters)
        {
            var foldersQuery = _dbContext
                .Folders
                .OrderBy(x => x.Name)
                .Include(x => x.Items)
                .AsQueryable();

            foldersQuery = foldersQuery
                .Where(x => x.IsHidden == filterParameters.IsHidden);

            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                foldersQuery = foldersQuery
                    .Where(x => x.Name.Contains(filterParameters.Name));
            }

            var folders = await foldersQuery.ToListAsync();
            return _mapper.Map<List<FolderViewModel>>(folders);
        }

        public async Task<FolderViewModel> GetFolder(Guid id)
        {
            var folder = await _dbContext
                .Folders
                .Include(x => x.Items)
                .FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<FolderViewModel>(folder);
        }

        public async Task<Guid> Create(FolderViewModel model)
        {
            var newFolder = _mapper.Map<FolderEntity>(model);
            newFolder.Id = Guid.NewGuid();
            _dbContext.Folders.Add(newFolder);
            await _dbContext.SaveChangesAsync();
            return newFolder.Id;
        }

        public async Task<Guid> Copy(Guid folderId)
        {
            var entity = await _dbContext.Folders
                .FirstOrDefaultAsync(x => x.Id == folderId);
            if (entity == null)
            {
                throw new ArgumentException("Not found");
            }

            entity.Name = entity.Name + " - Copy";
            entity.IsHidden = false;
            entity.Id = Guid.NewGuid();
            _dbContext.Folders.Add(entity);
            await _dbContext.SaveChangesAsync();

            var tasksEntities = await _dbContext.Items
                .Where(x => x.FolderId == folderId).ToListAsync();
            if (tasksEntities.Any())
            {
                foreach (var task in tasksEntities)
                {
                    task.Id = Guid.NewGuid();
                    task.StartDate = DateTime.Now;
                    task.FolderId = entity.Id;
                    task.Status = StatusItem.NotStarted;
                    task.CompletionDate = null;
                    _dbContext.Items.Add(task);
                }

                await _dbContext.SaveChangesAsync();
            }

            return entity.Id;
        }

        public async Task Update(Guid folderId, FolderViewModel model)
        {
            var entity = await _dbContext.Folders
                .FirstOrDefaultAsync(x => x.Id == folderId);
            if (entity == null)
            {
                throw new ArgumentException("Not found");
            }

            entity.Name = model.Name;
            entity.Icon = model.Icon;
            entity.IsHidden = model.IsHidden;

            _dbContext.Folders.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var findFolder = await _dbContext.Folders
                .FirstOrDefaultAsync(x => x.Id == id);
            if (findFolder == null)
            {
                return;
            }

            _dbContext.Folders.Remove(findFolder);
            await _dbContext.SaveChangesAsync();
        }
    }
}