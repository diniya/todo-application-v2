﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using todo_aspnetmvc.Models;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Services
{
    public interface IItemService
    {
        /// <summary>
        /// Returns item by Id
        /// </summary>
        /// <param name="itemId">Id of item</param>
        /// <returns></returns>
        Task<ItemViewModel> GetItem(Guid itemId);

        /// <summary>
        /// Creates new item
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> Create(ItemViewModel model);

        /// <summary>
        /// Updates item
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Update(ItemViewModel model);

        /// <summary>
        /// Updates item
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task UpdateStatus(Guid itemId, StatusItem status);

        /// <summary>
        /// Deletes item by Id
        /// </summary>
        /// <param name="itemId">Id of item</param>
        /// <returns></returns>
        Task Delete(Guid itemId);

        /// <summary>
        /// Returns items by folderId
        /// </summary>
        /// <param name="folderId">id of list</param>
        /// <param name="filterParameters"></param>
        /// <returns></returns>
        Task<List<ItemViewModel>> GetFolderItems(Guid folderId, ItemFilterParameters filterParameters);

        /// <summary>
        /// get count of today not completed tasks 
        /// </summary>
        /// <returns></returns>
        int GetTasksCount();

        /// <summary>
        /// get items' list of today not completed tasks 
        /// </summary>
        /// <returns></returns>
        Task<List<ItemViewModel>> GetTodayItems();
    }
}