﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using todo_aspnetmvc.Models;

namespace todo_aspnetmvc.Services
{
    public interface IFolderService
    {
        /// <summary>
        /// Returns list of folders
        /// </summary>
        /// <param name="filterParameters">Sorting and filtering parameters</param>
        /// <returns></returns>
        Task<List<FolderViewModel>> GetFolders(ItemFilterParameters filterParameters);

        /// <summary>
        /// Returns folder by Id
        /// </summary>
        /// <param name="id">Id of list</param>
        /// <returns></returns>
        Task<FolderViewModel> GetFolder(Guid id);

        /// <summary>
        /// Creates new Folder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> Create(FolderViewModel model);

        /// <summary>
        /// Updates Folder
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Update(Guid folderId, FolderViewModel model);

        /// <summary>
        /// Deletes folder by Id
        /// </summary>
        /// <param name="id">folder id</param>
        /// <returns></returns>
        Task Delete(Guid id);

        /// <summary>
        /// Copy folder 
        /// </summary>
        /// <param name="folderId">folder id</param>
        /// <returns></returns>
        Task<Guid> Copy(Guid folderId);
    }
}