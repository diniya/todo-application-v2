﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using todo_aspnetmvc.Models;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Services
{
    public class ItemService : IItemService
    {
        private readonly AppDbContext _dbContext;
        private readonly IMapper _mapper;

        public ItemService(AppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public int GetTasksCount()
        {
            return _dbContext.Items
                .Count(i => i.Folder != null && i.CompletionDate == DateTime.Now.Date && i.Status == StatusItem.NotStarted && !i.Folder.IsHidden);
        }

        public async Task<List<ItemViewModel>> GetFolderItems(Guid folderId, ItemFilterParameters filterParameters)
        {
            var itemsQuery = _dbContext.Items
                .Where(x => x.FolderId == folderId)
                .AsQueryable();
            if (filterParameters.Status.HasValue)
            {
                itemsQuery = itemsQuery
                    .Where(x => x.Status == filterParameters.Status.Value);
            }
          
            if (filterParameters.CompletionDate.HasValue)
            {
                itemsQuery = itemsQuery
                    .Where(x => x.CompletionDate == filterParameters.CompletionDate.Value);
            }

            
            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                itemsQuery = itemsQuery
                    .Where(x => x.Name.Contains(filterParameters.Name));
            }

            var items = await itemsQuery.ToListAsync();
            return _mapper.Map<List<ItemViewModel>>(items);
        }

        public async Task<List<ItemViewModel>> GetTodayItems()
        {
            var itemsQuery = _dbContext.Items
                .Where(x => x.Folder != null && x.CompletionDate == DateTime.Now.Date && !x.Folder.IsHidden)
                .AsQueryable();
            var items = await itemsQuery.ToListAsync();
            return _mapper.Map<List<ItemViewModel>>(items);
        }

        public async Task<ItemViewModel> GetItem(Guid itemId)
        {
            var item = await _dbContext.Items
                .Where(x => x.Id == itemId)
                .FirstOrDefaultAsync();
            return _mapper.Map<ItemViewModel>(item);
        }

        public async Task<Guid> Create(ItemViewModel model)
        {
            var entity = _mapper.Map<ItemEntity>(model);
            entity.Id = Guid.NewGuid();
            entity.StartDate = DateTime.Now;
            _dbContext.Items.Add(entity);
            await _dbContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task Update(ItemViewModel model)
        {
            var entity = await _dbContext.Items
                .FirstOrDefaultAsync(x => x.Id == model.Id);
            if (entity == null)
            {
                throw new ArgumentException("Not found");
            }

            entity.Name = model.Name;
            entity.Description = model.Description;
            entity.CompletionDate = model.CompletionDate;

            _dbContext.Items.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateStatus(Guid itemId, StatusItem status)
        {
            var entity = await _dbContext.Items
                .FirstOrDefaultAsync(x => x.Id == itemId);
            if (entity == null)
            {
                throw new ArgumentException("Not found");
            }

            entity.Status = status;

            _dbContext.Items.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Guid itemId)
        {
            var findTask = await _dbContext.Items
                .FirstOrDefaultAsync(x => x.Id == itemId);
            if (findTask == null)
            {
                return;
            }

            _dbContext.Items.Remove(findTask);
            await _dbContext.SaveChangesAsync();
        }
    }
}