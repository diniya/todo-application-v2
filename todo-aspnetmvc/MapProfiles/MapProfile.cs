﻿using AutoMapper;
using todo_aspnetmvc.Models;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.MapProfiles
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<ItemEntity, ItemViewModel>().ReverseMap();
            CreateMap<FolderEntity, FolderViewModel>().ReverseMap();
        }
    }
}