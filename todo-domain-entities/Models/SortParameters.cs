﻿namespace todo_domain_entities.Models
{
    public class SortParameters
    {
        public string SortField { get; set; }
        public bool SortType { get; set; }
    }
}