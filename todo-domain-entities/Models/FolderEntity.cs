﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace todo_domain_entities.Models
{
    public class FolderEntity
    {
        public Guid Id { get; set; }

        [Required] public string Name { get; set; }
        public bool IsHidden { get; set; }
        public string? Icon { get; set; }
        public List<ItemEntity> Items { get; set; } = new List<ItemEntity>();
    }
}