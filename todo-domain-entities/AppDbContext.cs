﻿using Microsoft.EntityFrameworkCore;
using todo_domain_entities.Models;

namespace todo_domain_entities
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<FolderEntity> Folders { get; set; }
        public DbSet<ItemEntity> Items { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FolderEntity>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<FolderEntity>()
                .Property(x => x.IsHidden).HasDefaultValue(false);

            modelBuilder.Entity<ItemEntity>().HasKey(x => x.Id);
            modelBuilder.Entity<ItemEntity>()
                .Property(x => x.Status).HasDefaultValue(StatusItem.NotStarted);

            modelBuilder.Entity<FolderEntity>()
                .HasMany(x => x.Items)
                .WithOne(x => x.Folder)
                .HasForeignKey(x => x.FolderId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}